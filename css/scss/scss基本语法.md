### 变量声明
    1、使用 $ 符声明变量
    2、变量名用中划线和下划线一样(看看个人使用习惯)
        如 $highlight-color 和 $hightlight_color 其实是同一个变量
    3、作用域
       如果变量定义在规则块({})内，那么该变量只能在{}内使用
    4、插值语句: #{}

### 嵌套规则
    1、可以根据元素的父子关系嵌套的书写规则，提高可读性，减少代码量
        ul{
            // css
            li{
                //css
            }
        }
    2、父选择器标识符&, 当包含父选择器标识符的嵌套规则被打开时，它不会像后代选择器那样进行拼接，而是&被父选择器直接替换
        a{
            &:hover{
                //css
            }
        }
    3、属性嵌套
        nav {
            border: {
                style: solid;
                width: 1px;
                color: #ccc;
            }
        }

### 混合器
    1、属性混合
     定义：
     @mixin rounded-corners {
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border-radius: 5px;
    }
    使用：
    .notice {
        background-color: green;
        border: 2px solid #00aa00;
        @include rounded-corners;
    }
    结果：
    .notice {
        background-color: green;
        border: 2px solid #00aa00;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border-radius: 5px;
    }
    2、混合器还可以包含选择器
        定义：
        @mixin no-bullets {
            list-style: none;
            li {
                list-style-image: none;
                list-style-type: none;
                margin-left: 0px;
            }
        }
        使用：
        ul.plain {
            color: #444;
            @include no-bullets;
        }
        结果：
        ul.plain {
            color: #444;
            list-style: none;
        }
        ul.plain li {
            list-style-image: none;
            list-style-type: none;
            margin-left: 0px;
        }
    3、混合器传参
        定义：
        ul.plain {
            color: #444;
            list-style: none;
        }
        ul.plain li {
            list-style-image: none;
            list-style-type: none;
            margin-left: 0px;
        }
        使用：
        a {
            @include link-colors(blue, red, green);
        }
        结果：
        a { color: blue; }
        a:hover { color: red; }
        a:visited { color: green; }
    4、默认值
        @mixin link-colors(
            $normal,
            $hover: $normal,
            $visited: $normal
        )
        {
        color: $normal;
        &:hover { color: $hover; }
        &:visited { color: $visited; }
        }

        使用：
        a {
            color: #444;
            @include link-colors(red);
        }

### 继承
    .error {
        border: 1px solid red;
        background-color: #fdd;
    }
    .seriousError {
        @extend .error;
        border-width: 3px;
    }

### 函数

    quote(string)  给字符串添加引号

    abs(number) 返回一个数值的绝对值
    ceil(number) 向上取整
    comparable(num1, num2)	返回一个布尔值，判断 num1 与 num2 是否可以进行比较
    floor(number)	向下取整
    max(number...)	返回最大值
    min(number...)	返回最小值
    percentage(number)	将数字转化为百分比的表达形式
    random()	返回 0-1 区间内的小数，
    random(number)	返回 1 至 number 之间的整数，包括 1 和 limit
    round(number)	返回最接近该数的一个整数，四舍五入。

    append(list, value, [separator])	将单个值 value 添加到列表尾部。separator 是分隔符，默认会自动侦测，或者指定为逗号或空格。
    index(list, value)	返回元素 value 在列表中的索引位置。
    is-bracketed(list)	判断列表中是否有中括号

    hsl(hue, saturation, lightness)	通过色相（hue）、饱和度(saturation)和亮度（lightness）的值创建一个颜色
    hsla(hue, saturation, lightness, alpha)	通过色相（hue）、饱和度(saturation)、亮度（lightness）和透明（alpha）的值创建一个颜色。
    grayscale(color)	将一个颜色变成灰色，相当于 desaturate( color,100%)。
    ......

### 控制指令
    1、@if
        p {
            @if 1 + 1 == 2 { border: 1px solid; }
            @if 5 < 3 { border: 2px dotted; }
            @if null  { border: 3px double; }
        }

        $type: monster;
        p {
            @if $type == ocean {
                color: blue;
            } @else if $type == matador {
                color: red;
            } @else if $type == monster {
                color: green;
            } @else {
                color: black;
            }
        }
    2、@for
        @for 指令可以在限制的范围内重复输出格式，每次按要求（变量的值）对输出结果做出变动。这个指令包含两种格式：@for $var from <start> through <end>，或者 @for $var from <start> to <end>，区别在于 through 与 to 的含义：当使用 through 时，条件范围包含 <start> 与 <end> 的值，而使用 to 时条件范围只包含 <start> 的值不包含 <end> 的值。另外，$var 可以是任何变量，比如 $i；<start> 和 <end> 必须是整数值。

        @for $i from 1 through 3 {
            .item-#{$i} { width: 2em * $i; }
        }
    3、@each
        @each 指令的格式是 $var in <list>, $var 可以是任何变量名，比如 $length 或者 $name，而 <list> 是一连串的值，也就是值列表。

        @each 将变量 $var 作用于值列表中的每一个项目，然后输出结果，例如：

        @each $animal in puma, sea-slug, egret, salamander {
        .#{$animal}-icon {
            background-image: url('/images/#{$animal}.png');
        }
        }
        编译为

        .puma-icon {
        background-image: url('/images/puma.png'); }
        .sea-slug-icon {
        background-image: url('/images/sea-slug.png'); }
        .egret-icon {
        background-image: url('/images/egret.png'); }
        .salamander-icon {
        background-image: url('/images/salamander.png'); }

    4、@while
        $i: 6;
        @while $i > 0 {
        .item-#{$i} { width: 2em * $i; }
        $i: $i - 2;
        }

    5、@function
        Sass 支持自定义函数，并能在任何属性值或 Sass script 中使用：
        $grid-width: 40px;
        $gutter-width: 10px;

        @function grid-width($n) {
        @return $n * $grid-width + ($n - 1) * $gutter-width;
        }

        #sidebar { width: grid-width(5); }
        编译为

        #sidebar {
        width: 240px; }

